The genomes for cactus test can be found in:

/nfs/production/mousegenomes/projects/InbredStrains/Assembly/*.fasta

GRCm38.fasta is the reference

A_J, ARK_J, BALB_cJ, C3H_HeJ, C57BL_6NJ, CAST_EiJ, CBA_J, DBA_2J, FVB_NJ are other mouse strains, distance 0.5M - 1M years to reference

CAROLI_EiJ is another mouse species, 3M years to reference

mOncTor is outgroup, ~ 35M years to reference 
