output "floating_ip" {
  description = "the public IP number of the bastion host"
  value       = "${openstack_networking_floatingip_v2.floatingip.address}"
}

output "network_id" {
  description = "the ID of the network"
  value = "${openstack_networking_network_v2.network.id}"
}

output "network" {
  description = "the name of the network"
  value = "${openstack_networking_network_v2.network.name}"
}

output "subnet_id" {
  value = "${openstack_networking_subnet_v2.subnet.id}"
}

output "router_id" {
  value = "${openstack_networking_router_v2.router.id}"
}

output "interface_id" {
  value = "${openstack_networking_router_interface_v2.interface.id}"
}

output "security_groups" {
  value = [ "${local.default_security_group}", "${local.namespace}-firewall", "${local.namespace}-LSF" ]
}

output "secgroup.LSF.id" {
  value = "${openstack_compute_secgroup_v2.LSF.id}"
}

output "secgroup.firewall.id" {
  value = "${openstack_compute_secgroup_v2.firewall.id}"
}

output "key_pair" {
  description = "The name of the auto-generated keypair"
  value       = "${openstack_compute_keypair_v2.keypair.name}"
}
