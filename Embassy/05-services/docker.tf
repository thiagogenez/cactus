resource "null_resource" "docker_bastion" {

  connection {
    user        = "${local.user}"
    private_key = "${file(local.private_key)}"
    agent       = false
    host        = "${local.floating_ip}"
  }

  # triggers { something = "${uuid()}" }

  provisioner "remote-exec" {
    inline = [
      "sudo /home/${local.user}/bin/docker.sh",
    ]
  }
}

resource "null_resource" "docker_hosts" {

  connection {
    user        = "${local.user}"
    private_key = "${file(local.private_key)}"
    agent       = false
    host                = "${element(local.host_ips, count.index)}"
    bastion_private_key = "${file(local.private_key)}"
    bastion_host        = "${local.floating_ip}"
    bastion_user        = "${local.user}"
  }

  count = "${local.host_count}"

  # triggers { something = "${uuid()}" }

  provisioner "remote-exec" {
    inline = [
      "sudo /home/${local.user}/bin/docker.sh",
    ]
  }
}
