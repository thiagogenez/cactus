data "terraform_remote_state" "state" {
  backend = "local"
  config {
    path = "../00-config/terraform.tfstate"
  }
}

data "terraform_remote_state" "vpc" {
  backend = "local"
  config {
    path = "../01-vpc/terraform.tfstate"
  }
}

data "terraform_remote_state" "bastion" {
  backend = "local"
  config {
    path = "../03-bastion/terraform.tfstate"
  }
}

data "terraform_remote_state" "hosts" {
  backend = "local"
  config {
    path = "../04-hosts/terraform.tfstate"
  }
}

locals {
  state = "../${data.terraform_remote_state.state.state}"

  namespace = "${data.terraform_remote_state.state.namespace}"

  bastion_ip = "${data.terraform_remote_state.bastion.bastion_ip}"
  floating_ip = "${data.terraform_remote_state.vpc.floating_ip}"

  public_key = "${data.terraform_remote_state.state.public_key}"
  private_key = "${data.terraform_remote_state.state.private_key}"
  key_pair = "${data.terraform_remote_state.vpc.key_pair}"

  user = "${data.terraform_remote_state.state.tenancy_user}"
  host_count = "${data.terraform_remote_state.state.host_count}"
  host_ips = "${data.terraform_remote_state.hosts.host_ips}"

  lsf_source = "${data.terraform_remote_state.state.lsf_source}"
  lsf_host_range = "${data.terraform_remote_state.state.lsf_host_range}"
  lsf_version = "${data.terraform_remote_state.state.lsf_version}"
}
