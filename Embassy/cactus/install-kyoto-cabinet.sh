#!/bin/bash

sudo yum install -y gcc gcc-c++ zlib-devel

set -ex
NPROCS=`cat /proc/cpuinfo | grep -c processor`
# Install kyoto cabinet
dir=/home/user/cactus/kyoto
mkdir -p $dir
cd $dir

kcvsn=1.2.77
kcdir=kyotocabinet-${kcvsn}
if [ ! -d ${kcdir} ]; then
  wget http://fallabs.com/kyotocabinet/pkg/kyotocabinet-${kcvsn}.tar.gz
  tar xf kyotocabinet-${kcvsn}.tar.gz
  cd ${kcdir}
  ./configure
  make -j $NPROCS
  # make check
else
  cd ${kcdir}
fi
sudo sudo make install

# Install kyoto tycoon
cd $dir
ktvsn=0.9.56
ktdir=kyototycoon-${ktvsn}
if [ ! -d ${ktdir} ]; then
  wget http://fallabs.com/kyototycoon/pkg/kyototycoon-${ktvsn}.tar.gz
  tar xf kyototycoon-${ktvsn}.tar.gz
  cd ${ktdir}
  ./configure

  mv ktdbext.h{,.sav}
  (
    echo '#include <unistd.h>'
    cat ktdbext.h.sav
  ) | tee ktdbext.h >/dev/null
  make -j $NPROCS
else
  cd ${ktdir}
fi
sudo make install

echo "export LD_LIBRARY_PATH=/usr/local/lib" | sudo tee /etc/profile.d/kyoto.sh

# Test ktserver
# LD_LIBRARY_PATH=/usr/local/lib /usr/local/bin/ktserver --versions