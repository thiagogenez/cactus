#!/bin/bash

dir=/home/user/cactus/
source $dir/cactus_env/bin/activate

cd $dir
export PATH=${PATH}:$dir/cactus/bin
time cactus example_SM_local ./cactus/examples/evolverMammals.txt ./example_SM_local/result.hal --stats --batchSystem singleMachine  --binariesMode local
