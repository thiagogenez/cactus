output "host_ips" {
  description = "id of the selected image"
  value       = "${openstack_compute_instance_v2.host.*.access_ip_v4}"
}
