resource "openstack_blockstorage_volume_v2" "bastion_volumes" {
  count = "${length(local.bastion_volumes)}"

  name = "${local.namespace}-${lower( lookup( local.bastion_volumes[count.index], "label" ) )}"
  size =        "${lookup( local.bastion_volumes[count.index], "size" )}"

  metadata = {
    # id     = "${self.id}",
    name   = "${lower( lookup( local.bastion_volumes[count.index], "label" ) )}",
    size   = "${lookup( local.bastion_volumes[count.index], "size"   )}",
    mount  = "${lookup( local.bastion_volumes[count.index], "mount"  )}",
    label  = "${lookup( local.bastion_volumes[count.index], "label"  )}",
    export = "${lookup( local.bastion_volumes[count.index], "export", "ro" )}",
    mode   = "${lookup( local.bastion_volumes[count.index], "mode",   "-" )}",
    group  = "${lookup( local.bastion_volumes[count.index], "group",  "-" )}",
    count  = "${count.index}"
  }
}
