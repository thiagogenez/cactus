data "terraform_remote_state" "state" {
  backend = "local"
  config {
    path = "../00-config/terraform.tfstate"
  }
}

data "terraform_remote_state" "vpc" {
  backend = "local"
  config {
    path = "../01-vpc/terraform.tfstate"
  }
}

locals {
  namespace = "${data.terraform_remote_state.state.namespace}"

  user = "${data.terraform_remote_state.state.tenancy_user}"
  floating_ip = "${data.terraform_remote_state.vpc.floating_ip}"

  public_key = "${data.terraform_remote_state.state.public_key}"
  private_key = "${data.terraform_remote_state.state.private_key}"

  bastion_volumes = "${data.terraform_remote_state.state.bastion_volumes}"
}
