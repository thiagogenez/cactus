#!/bin/bash

file=$1
if [ "$file" == "" ]; then
  echo "Need a json file with user information"
  exit 1
fi

length=`cat $file | jq length`
if [ "$length" == "" ]; then
  echo "No user information, exiting..."
  exit 0
fi

index=0
while [ $index -lt $length ]
do
  user=`  cat $file | jq .[${index}].user   | tr -d '"'`
  id=`    cat $file | jq .[${index}].id     | tr -d '"'`
  sudo=`  cat $file | jq .[${index}].sudo   | tr -d '"'`
  groups=`cat $file | jq .[${index}].groups | tr -d '"'`
  home=`  cat $file | jq .[${index}].home   | tr -d '"'`

  echo "Add $user($id) to bastion and hosts with sudo=$sudo and groups=$groups"
  index=`expr 1 + $index`

  options=""
  if [ ! "$id" == "null" ]; then
    options="$options --uid $id"
  fi
  if [ "$home" == "null" ]; then
    home="/home/$user"
  fi

  echo "Install $user($id) on host `hostname -s`"


  # Check if the home directory already exists -> shared filesystem
  clobber="yes"
  if [ -d $home ]; then
    clobber="no"
  fi

  id $user || id $id || adduser $options --user-group $user --home-dir $home
  for group in $groups
  do
    if [ $group != '[' ] && [ $group != ']' ]; then
      groupadd --force $group
      usermod --append --groups $group $user
    fi
  done
  if [ "$sudo" == "yes" ]; then
    sudo_file=/etc/sudoers.d/99-add-users
    touch $sudo_file
    mv $sudo_file{,.tmp}
    echo "$user ALL=(ALL) NOPASSWD:ALL" | tee -a $sudo_file.tmp >/dev/null
    cat $sudo_file.tmp | sort | uniq | tee $sudo_file
    rm -f $sudo_file.tmp
  fi

  if [ "$clobber" == "yes" ]; then
    ssh_dir="${home}/.ssh/"
    # [ -d $ssh_dir ] && rm -rf $ssh_dir # Don't nuke the .ssh directory in the bastion!
    key_dir=".ssh/keys"
    mkdir -p $ssh_dir
    chmod 700 $ssh_dir
    restorecon -r -v -F $ssh_dir || /bin/true # Ignore failures in case selinux not in use

    cp $key_dir/key.$user.pub $ssh_dir/authorized_keys
    mv $key_dir/key.$user $ssh_dir/key
    mv $key_dir/key.$user.pub $ssh_dir/key.pub
    if [ -f $key_dir/key.$user.pub.provided ]; then
      cat $key_dir/key.$user.pub.provided | tee -a $ssh_dir/authorized_keys >/dev/null
      mv $key_dir/key.$user.pub.provided $ssh_dir/key.pub.provided
    fi

    (
      echo "Host *"
      echo "  IdentityFile \"$ssh_dir/key\""
      echo "  StrictHostKeyChecking no"
      echo "  UserknownHostsFile /dev/null"
    ) | tee $ssh_dir/config >/dev/null

    chmod 600 $ssh_dir/authorized_keys $ssh_dir/config $ssh_dir/key
    chown -Rf $user.$user $ssh_dir
  fi

done

echo "All done..."
