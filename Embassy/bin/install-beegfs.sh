#!/bin/bash

master=$1
if [ "$master" == "" ]; then
  echo "Need the master host name"
  exit 1
fi

admin=$2
if [ "$admin" != "" ]; then
  echo "I am an admin node"
fi

if [ "`grep EPHEMERAL0 /etc/fstab`" != "" ]; then
  fstab="/etc/fstab"
  mv $fstab{,.new}
  cat $fstab.new | sed -e 's%/EPHEMERAL0%/beegfs%' | tee $fstab >/dev/null
  /bin/rm -f $fstab.new

  umount /EPHEMERAL0
  mv /EPHEMERAL0 /beegfs
  mount /beegfs
fi

yum update -y
yum install -y wget kernel-devel

repo="/etc/yum.repos.d/beegfs-rhel6.repo"
if [ ! -f $repo ]; then
  wget -O $repo https://www.beegfs.io/release/latest-stable/dists/beegfs-rhel6.repo
  rpm --import https://www.beegfs.io/release/latest-stable/gpg/RPM-GPG-KEY-beegfs
fi

admin_pkgs="beegfs-mgmtd beegfs-admon beegfs-mon"
pkgs="beegfs-meta beegfs-storage beegfs-client beegfs-helperd beegfs-utils"
host=`hostname`
if [ "$admin" != "" ]; then
  pkgs="$admin_pkgs $pkgs"
fi
yum install -y $pkgs

mkdir -p /beegfs/host/{mgmtd,meta,storage} /beegfs/data
echo "/beegfs/data /etc/beegfs/beegfs-client.conf" | tee /etc/beegfs/beegfs-mounts.conf
/etc/init.d/beegfs-client rebuild
echo "If that failed, reboot, make sure you're running a kernel for which kernel-devel is installed, and try again"

cd /etc/beegfs

if [ "$admin" != "" ]; then
  /opt/beegfs/sbin/beegfs-setup-mgmtd -p /beegfs/host/mgmtd
  /opt/beegfs/sbin/beegfs-setup-meta -p /beegfs/host/meta -s 2 -m $master
fi
/opt/beegfs/sbin/beegfs-setup-storage -p /beegfs/host/storage -s 1 -i 101 -m $master
/opt/beegfs/sbin/beegfs-setup-client -m $master

for f in `egrep -l ^sysMgmtdHost beegfs-*.conf`
do
  if [ "`egrep sysMgmtdHost $f | egrep $master`" != "" ]; then
    echo "MgmtHost already set in $f"
  else
    echo "set MgmtHost in $f"
    /bin/mv $f{,.sav}
    cat $f.sav | sed -e "s%sysMgmtdHost *=.*$%sysMgmtdHost = $master%" | tee $f >/dev/null
    /bin/rm $f.sav
  fi
done

if [ "$admin" != "" ]; then
  systemctl start beegfs-mgmtd
  systemctl start beegfs-meta
fi
systemctl start beegfs-storage
systemctl start beegfs-helperd
systemctl start beegfs-client
if [ "$admin" != "" ]; then
  systemctl start beegfs-admon
fi

# beegfs-ctl --listnodes --nodetype=meta --nicdetails
# beegfs-ctl --listnodes --nodetype=storage --nicdetails
# beegfs-ctl --listnodes --nodetype=client --nicdetails
# beegfs-net            # Displays connections the client is actually using
beegfs-check-servers  # Displays possible connectivity of the services
beegfs-df