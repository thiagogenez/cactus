#!/bin/bash

yum remove -y `rpm -qa | egrep mariadb`
yum install -y http://repo.mysql.com/mysql-community-release-el7-5.noarch.rpm
yum install -y mysql-server python-devel mysql-devel
pip install mysqlclient
