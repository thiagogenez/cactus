#!/bin/bash
#
# This script creates /etc/hosts entries suitable for dnsmasq on the bastion
#
state=$1
namespace=$2
if [ "$namespace" == "" ]; then
  echo "Need the state directory and namespace"
  exit 1
fi

cd $state
state=`pwd`

cd fragments/ip
for f in `find . -type f | sed -e 's%^\.\/%%'`
do
  host=`cat $f | sed -e "s%^${namespace}-%%"`
  ip=`echo $f | tr '/' '.'`
    echo "${ip} ${host} ${namespace}-${host}"
done | sort | tee $state/hosts.dnsmasq
