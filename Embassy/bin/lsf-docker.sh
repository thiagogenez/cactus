#!/bin/bash

vsn=$1
if [ "$vsn" == "" ]; then
  echo "Need the LSF version number"
  exit 1
fi

cluster=$2
if [ "$cluster" == "" ]; then
  echo "Need a cluster name"
  exit 1
fi

dest=/lsf/${vsn}

if [ `hostname` == "bastion" ]; then
  (
    echo "LSF_PROCESS_TRACKING=Y"
    echo "LSF_LINUX_CGROUP_ACCT=Y"
    echo "LSB_RESOURCE_ENFORCE=\"cpu memory\""
  ) | tee -a ${dest}/conf/lsf.conf

# Now the cluster-specific parts
  cluster_conf=${dest}/conf/lsf.cluster.${cluster}
  mv $cluster_conf{,.orig}
  cat ${cluster_conf}.orig \
    | sed \
      -e "s%) *$% docker)%" \
    | tee $cluster_conf

  shared=${dest}/conf/lsf.shared
  mv $shared{,.orig}
  cat $shared.orig \
    | egrep -v "^#[A-Z]|^# *[^ ]* *(Boolean|Numeric|String) *" \
    | awk "{ print }/^RESOURCENAME/ { print \"   docker     Boolean ()       ()          (Docker container)\" }" \
    | tee $shared

  apps=${dest}/conf/lsbatch/${cluster}/configdir/lsb.applications
  docker_starter=`find $dest/${vsn} -name docker-starter.py`
  [ -f $docker_starter ] || ( echo "Cannot find docker_starter.py ($docker_starter), spitting the dummy"; exit 1 )
  docker_control=`find $dest/${vsn} -name docker-control.py`
  [ -f $docker_control ] || ( echo "Cannot find docker_control.py ($docker_control), spitting the dummy"; exit 1 )
  docker_monitor=`find $dest/${vsn} -name docker-monitor.py`
  [ -f $docker_monitor ] || ( echo "Cannot find docker_monitor.py ($docker_monitor), spitting the dummy"; exit 1 )
  (
    echo " "
    echo "Begin Application"
    echo "NAME = dockerapp"
    echo "DESCRIPTION = Enables job running in Docker container"
    echo "CONTAINER = docker[image(ubuntu) options(--rm)]"
    echo "EXEC_DRIVER = context[user(lsfadmin)] starter[$docker_starter] controller[$docker_control] monitor[$docker_monitor]"
    echo "End Application"
  ) | tee -a $apps

  sudo chown lsfadmin.lsfadmin $docker_starter $docker_control $docker_monitor
  sudo chmod 700 $docker_starter $docker_control $docker_monitor

  cd /lsf
  cwlvsn=0.2.1
  wget https://github.com/IBMSpectrumComputing/cwlexec/releases/download/v${cwlvsn}/cwlexec-${cwlvsn}.tar.gz
  tar xvf cwlexec-${cwlvsn}.tar.gz
fi

echo "export PATH=\${PATH}:/lsf/cwlexec-0.2.1" | tee /etc/profile.d/cwlexec.sh
echo "setenv PATH \${PATH}:/lsf/cwlexec-0.2.1" | tee /etc/profile.d/cwlexec.csh

#
# This part is common to all nodes, bastion plus workers
pip install cwltest

if [ `hostname` == "bastion" ]; then # Launch the integration test suite, just for fun...
  cd ~lsfuser
  git clone https://github.com/IBMSpectrumComputing/cwlexec
  chown -Rf lsfuser.lsfuser cwlexec
  cd cwlexec/src/test/integration-test
  su lsfuser -c ./run.sh 2>&1 | su lsfuser -c tee run.sh.log >/dev/null &
  disown %1
  
  echo "Re-starting LSF daemons"
  source /etc/profile.d/lsf.sh
  lsadmin reconfig -f
  badmin mbdrestart
fi

source /etc/profile.d/lsf.sh
lsf_daemons restart
lsf_daemons status
