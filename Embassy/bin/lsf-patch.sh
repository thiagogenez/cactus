#!/bin/bash

source /etc/profile.d/lsf.sh

set -e
vsn=$1
if [ "$vsn" == "" ]; then
  echo "Need the LSF version number"
  exit 1
fi

patchfile=$2
if [ "$patchfile" == "" ]; then
  echo "Need the LSF patch tarball"
  exit 1
fi

patchdir=`dirname $patchfile`
if [ ! -d $patchdir ]; then
  echo "No patch directory, not installing..."
  exit 0
fi

cd $patchdir || ( echo "Cannot cd to patchfile directory $patchdir"; exit 1 )
patchfile=`basename $patchfile`

dest=/lsf/${vsn}

dir=RH7_X64
[ -f $patchfile ] || ( echo "$patchfile not found"; exit 1 )
[ -d $dir ] && rm -rf $dir

echo "Unpacking $patchfile"
tar xf $patchfile
cd $dir
export LSF_TOP=$dest
$LSF_TOP/${vsn}/install/pversions

for build in `ls lsf*.tar.Z | sed -e 's%^.*-%%' -e 's%.tar.Z$%%' | sort`
do
  echo "Looking for $build"
  patch=`ls lsf${vsn}_* | grep ${build}`
  echo "Found $patch"
  if [ -f $patch ]; then
    echo "  Install $patch"
    $LSF_TOP/${vsn}/install/patchinstall --silent $patch
  fi
done

lsf_daemons restart
