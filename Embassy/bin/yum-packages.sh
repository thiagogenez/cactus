#!/bin/sh

set +ex
yum -y install epel-release.noarch
yum group install -y 'Development Tools'
yum install -y kernel-devel nfs-utils bind-utils mailx ansible git wget python-pip python-devel anaconda jq sysstat ftp
yum update -y
exit 0
