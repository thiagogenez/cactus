#!/bin/bash
#
# This script records IP names/numbers locally, in a scalable manner.
#
# This is used to build an SSH config for the local machine, one for
# the bastion and worker nodes, and for the dnsmasq configuration.
#

state=$1
ip=$2
hostname=$3

if [ "$hostname" == "" ]; then
  echo "Need a directory, an IP address and a hostname"
  exit 1
fi

echo "Got state=$state, ip=$ip"
state="$state/fragments/ip"
ip=`echo $ip | tr '.' '/'`
dir=`dirname $state/$ip`
mkdir -p $dir
echo $hostname | tee $state/$ip >/dev/null
