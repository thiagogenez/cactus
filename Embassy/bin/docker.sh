#!/bin/bash

# install docker
yum install -y docker
systemctl enable docker

# set up the docker group and permissions
groupadd docker
usermod -aG docker lsfadmin
usermod -aG docker root
usermod -aG docker centos
usermod -aG docker linux

# - configure for CentOS
#   in /etc/sysconfig/docker-storage. change 'overlay2' to 'devicemapper'
#   - would do this, but it requires an external block device, so don't!
#   - besides, it doesn't work, since overlay2 is the (un-specified!) default
# mv /etc/sysconfig/docker-storage{,.sav}
# cat /etc/sysconfig/docker-storage.sav \
#   | sed -e 's%overlay2%storagemapper%' \
#   | tee /etc/sysconfig/docker-storage
# /bin/rm -f /etc/sysconfig/docker-storage.sav

#   in /etc/sysconfig/docker-storage-setup, STORAGE_DRIVER=devicemapper
mv /etc/sysconfig/docker-storage-setup{,.sav}
cat /etc/sysconfig/docker-storage-setup.sav \
  | egrep -v STORAGE_DRIVER \
  | tee /etc/sysconfig/docker-storage-setup
echo "STORAGE_DRIVER=devicemapper" | tee -a /etc/sysconfig/docker-storage-setup
/bin/rm -f /etc/sysconfig/docker-storage-setup.sav

#   disable selinux for docker
mv /etc/sysconfig/docker{,.sav}
cat /etc/sysconfig/docker.sav \
  | sed -e 's%--selinux-enabled[^ ]*%--selinux-enabled=false%' \
  | tee /etc/sysconfig/docker
/bin/rm -f /etc/sysconfig/docker.sav

# start docker
systemctl start docker
chmod 666 /var/run/docker.sock
systemctl status docker

