#!/bin/bash

ip2dec () {
    local a b c d ip=$@
    IFS=. read -r a b c d <<< "$ip"
    printf '%d\n' "$((a * 256 ** 3 + b * 256 ** 2 + c * 256 + d))"
}

dec2ip () {
    local ip dec=$@
    for e in {3..0}
    do
        ((octet = dec / (256 ** e) ))
        ((dec -= octet * 256 ** e))
        ip+=$delim$octet
        delim=.
    done
    printf '%s\n' "$ip"
}

ip=$1
incr=$2
if [ "$incr" == "" ]; then
  echo "Need an IP address and an increment"
  exit 1
fi

dec=`ip2dec $ip`
dec=`expr $dec + $incr`
dec2ip $dec
