#!/bin/bash

set -ex
file=$1
if [ "$file" == "" ]; then
  echo "Need a json file with mount point information"
  exit 1
fi

length=`cat $file | jq length`
if [ "$length" == "" ]; then
  echo "No mount-point information, exiting..."
  exit 0
fi

index=0
while [ $index -lt $length ]
do
  mode=`  cat $file | jq .[${index}].mode   | tr -d '"'`
  mount=` cat $file | jq .[${index}].mount  | tr -d '"'`
  group=` cat $file | jq .[${index}].group  | tr -d '"'`

  index=`expr 1 + $index`

  if [ ! "$mode" == "-" ] && [ ! "$mode" == "null" ]; then
    chmod $mode $mount
  fi
  if [ ! "$group" == "-" ] && [ ! "$group" == "null" ]; then
    if [ ! $(getent group $group) ]; then
      groupadd --group $group
    fi
    chgrp $group $mount
  fi
done

echo "All done..."
