#!/bin/bash

set -ex
json=$1
if [ "$json" == "" ]; then
  echo "Need a json configuration file"
  exit 1
fi

user=`          cat $json | jq .user           | tr -d '"'`
setup_key=`     cat $json | jq .setup_key      | tr -d '"'`
restrict_paths=`cat $json | jq .restrict_paths | tr -d '"'`

echo "Configure Globus Connect for user \"$user\" with paths \"$restrict_paths\""

file=globusconnectpersonal-latest.tgz
url=https://downloads.globus.org/globus-connect-personal/linux/stable/$file

cd /tmp
[ -f $file ] || wget -O $file $url
dest=`getent passwd gcuser | cut -d: -f 6`
cd $dest
su $user -c "tar xf /tmp/$file"
dir=`ls -d globusconnectpersonal-* | egrep '\-[0-9,.]+$'`
if [ "$dir" == "" ]; then
  echo "No directory found for Globus Connect Personal"
  exit 1
fi
if [ ! -d "$dir" ]; then
  echo "\"$dir\" is not a directory"
  exit 1
fi

echo "Found Globus Connect in \"$dir\""
cd $dir

pip install --upgrade globus-cli

if [ "$setup_key" == "" ]; then
  echo "No setup key, not installing Globus without it..."
  exit 0
fi
su $user -c "./globusconnect -setup $setup_key"

if [ "$restrict_paths" == "" ]; then
  echo "No path restrictions, not running Globus without it..."
  exit 0
fi

cp globus-connect.service /usr/lib/systemd/system/globus-connect.service
set up the configuration file somewhere

su $user -c "mkdir ~/bin; ln -s $dest/$dir/globusconnect $dest/bin/"
# su $user -c "./globusconnect -start -restrict-paths $restrict_paths 2>&1" | \
#   su $user -c "tee /tmp/globus-connect.log"
