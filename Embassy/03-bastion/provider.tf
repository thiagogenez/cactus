provider "openstack" {
  user_name   = "${data.terraform_remote_state.state.cloud_user}"
  password    = "${data.terraform_remote_state.state.password}"
  domain_name = "${data.terraform_remote_state.state.domain}"
  tenant_name = "${data.terraform_remote_state.state.tenancy}"
  auth_url    = "${data.terraform_remote_state.state.auth_url}"
  version     = "~> 1.8"
}
