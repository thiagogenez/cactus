resource "null_resource" "create_state" {
  provisioner "local-exec" {
    command = "rm -rf ../${var.state} && mkdir -p ../${var.state}"
  }
}
