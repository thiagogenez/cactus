namespace = "cactus"

external_network = "ext-net-37"
tenancy_user = "centos"

bastion_image = "centos7"
bastion_disk_gb = 50
bastion_flavour = "s1.medium"

lsf_version = 10.1
lsf_source = "../LSF" # Relative to the directory with the lsf terraform installation script

bastion_volumes = [
  {
    mount = "/lsf",
    size = 50,
    label = "LSF"
  },
  {
    mount = "/home/user",
    size = 200,
    label = "USER",
    export = "rw"
  },
  {
    mount = "/data",
    size = 3700,
    label = "DATA",
    export = "rw",
    mode = "3777",
    group = "cactus"
  },
]

host_image = "centos7"
host_disk_gb = 60
host_flavour = "s1.gargantuan"
host_count = 10

groups = [
  { name = "cactus" }
]

users = [
  {
    user = "lsfadmin",
    id   = "1001",
    sudo = "yes",
    home = "/home/user/lsfadmin",
  },
  {
    user = "lsfuser",
    id   = "1002",
    sudo = "no",
    home = "/home/user/lsfuser",
  },
  {
    user = "muffato",
    id   = "3018",
    sudo = "no",
    home = "/home/user/muffato",
    groups = [ "cactus" ],
    key    = "ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEA0ELDgnSQySZnbcUXeopy71uShULohFxQRoaJeGaYbiY3P+B+F1BYvKp0X7FRJRDTJj1JJL2aOWWEmp8b0xhpQ34SWeSZybzH1KZjAN+Pc/vUTI4HTQOlukbeIP9kDpF/C9WQurmyrnYZVuzQkiPSfDtB8IeSkmOmJxFJN/BoqvrnOjwwQWiqKZehIUebRqivoJPjU+cyKBlemCTdqS8iaNVr5s7CNCG/STWkYoMWnR9tXouHX1kp7SpDn0xG2Kz/kcPerALaivEjuahWgO2WpyAIRSnw3rSBka/CPzrWvNo2AvO+Npq1s5H+0jB71utUh9TfmeRRPEIShUB2oqj7RQ== muffato@login-r6-1.ebi.ac.uk"
  },
  # {
  #   user = "waakanni",
  #   id   = "5859",
  #   sudo = "no",
  #   home = "/home/user/waakanni",
  #   groups = [ "cactus" ],
  #   key    = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDJbfysJ8f16Ov0+TqjrJbswwkQgCZ0PmwrL+pmcP0jWULJOk94YAK24ouxxBgZExCHTnoE5qIN+dA8XEDcydg6kqpLpHBy46AFor0rv0F6xRUd9GPFe07RXFm+dUyKGSmr2rkt55QbrpwRp2Ad3lWhMav7Xx2ML/xTooEKA9jsqGtMDpKf1zIdm+Rh1WrCQGJgkX0RReFs5uCmeYa+Pxx+GBWHuuOKWRSUbqoUDbu7lanNvBTPhdnYFVPu6bAyua07nKI/i42nSpOPj0DaFoFExM7WcHZx6ESTnb6Mu4HSFOgf4C3PRTZWYscLdqJJDwCJaZE5j6hnTt+sj4H8Iz/Z waakanni@ebi.ac.uk"
  # },
  {
    user = "dthybert",
    id   = "2516",
    sudo = "no",
    home = "/home/user/dthybert",
    groups = [ "cactus" ],
    key    = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDBPC2wwxgR40gHFNPxToCE9tHykyF/L0np0r3RE0wbLADSOuQyY9qcRAQTTgLSE61uaEqnTE9YwhGcN8OBTybnHTwRGZz5DGPl19SGFnRvNs3nH74bzcHZxXzRC9DglN62ypPjBaH0nhbUwkNtwXVRxyEspNOnoAb6S2B0P6XYaA+o+PRzusvzm3tX/pl30i52UHlTk3DKHk2fdjJIREsovtm2XY4xlVPeK9kEanM8iYzR6lwVYt+R7fUN5zb6/ttsSv8s5Ug7DAAskfmlciYGifslBcwrAqjAPMRZFOk4ZHHA5xOfs1y2tNQfQJIxZji8xuDCiiWE+5dwuRervSC7 dthybert@FVFY803FMCKQ"
  }
  {
    user = "thiagogenez",
    id   = "9492",
    sudo = "no",
    home = "/home/user/thiagogenez",
    groups = [ "cactus" ],
    key    = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDjh2TH5mBvlCgKLtrmgCAUDBLrtaUwWtBhZE6OMxaSQpR2WiiGn7fxJlUbypm6w0R99O7iCOjdVk7ZORghGlz1PPzGntd3D8aON5yfS+UVeINHkDzT91WtBNyUb1LZf+XWKaF2zIb9Jz41a4V1OQ/Oa/xA4DkNHVyzXQL++LPZgQdBfwR8gs0C9tsPQLm6mDpbvw7GO7FzSU3t6mNdqDYbVlyq4ziywaMQWF+FviwlMNJKnXwUG04844CGJGM1LT3s0nv0caO+/dvdmVfysLNWCjekuhWsE1SpRgI7WM4M5elRS0EF75Urfp11N3Ro6q5VhtZh3V2Y7aVgA63hSqFvvJbxXiK8QGLib6UDIAIho+9MkCc+2DSdrhLd0hMxQJpXhs1t8+M0MlfUmbISI1ZLuRiliGTgyaMKoqmI61SnS1/c02nS0y+NDk1lW6vDUnSU9mqS7ZmVw/lC3/jMzDFtVqJkM+LNq3wIbnJluExeIk0WEAk10kICm/qI9Zwcn10DN2lLSVWdlDojOr4cnbAljb5DNZLugqGbfyroxndbzGWmHUQm5N16xc+XNWqZL6GY9/hc1nRVnsY2/Ek3QmBNN0V38d0ZnUTp1Nxox2TJVmZRqf1YrypA4UBy3a9VSLe+wKZkMF6qZc6GmP03DvrZSYByCitVR8jwyGKlbhC9mQ== thiagogenez@ebi.ac.uk"
  }
]