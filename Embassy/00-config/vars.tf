variable "namespace" {
  description = "Naming prefix for created resources, keeps them separate from other builds in the same tenancy"
  default     = "A"
}

variable "cloud_user" {
  description = "The login name to access the tenancy"
}

variable "password" {
  default = "Password to access the tenancy"
}

variable "tenancy" {
  description = "Name of the tenancy"
}

variable "domain" {
  description = "Tenancy domain, if needed"
  default = ""
}

variable "auth_url" {
  description = "Authorisation URL for the tenancy"
}

variable "availability_zone" {
  description = "Availability zone, optional"
  default = ""
}

variable "subnet" {
  description = "CIDR of the desired subnet for the VPC, e.g. 10.0.0.0/8 or 192.168.0.0/16"
  default     = "10.0.0.0/8"
}

variable "external_network" {
  description = "Name of the external network for the tenancy"
  default     = "admin_external_net"
}

variable "default_security_group" {
  description = "Default security group, optional"
  default = "default"
}

variable "state" {
  description = "Path to state information about the current build. This will be created and populated as needed, and should be relative to the top-level directory (above this one)"
  default = "state/"
}

variable "public_key" {
  description = "Name of the file where the public SSH key for your build will be stored, in the 'state' directory"
  default = "key.pub"
}

variable "private_key" {
  description = "Name of the file where the private SSH key for your build will be stored, in the 'state' directory"
  default = "key"
}

variable "tenancy_user" {
  description = "Login user for the compute instances on the tenancy. This user will be able to sudo to root"
}

variable "bastion_image" {
  description = "Name of the image for the bastion host"
}
variable "bastion_disk_gb" {
  description = "Size of the bastion root disk"
  default = "30"
}
variable "bastion_flavour" {
  description = "Bastion host flavor"
  default = "s1.tiny"
}

variable "lsf_source" {
  description = "Directory containing LSF installation packages. This is either an absolute path or a path relative to the *-install-lsf directory."
}
variable "lsf_version" {
  description = "LSF version, e.g. 10.1 as of August 2018"
}
variable "lsf_host_range" {
  description = "Inet host range for auto-join LSF hosts. Must overlap with the subnet cidr, above"
  default     = "10"
}

variable "bastion_volumes" {
  description = "Array of objects describing the disks to be attached to the bastion host and NFS-exported to the workers"
  default = []
}

variable "host_image" {
  description = "Name of the image for the worker nodes"
}
variable "host_disk_gb" {
  description = "Size of the worker node root disk"
  default = "30"
}
variable "host_flavour" {
  description = "Worker node flavour"
  default = "s1.large"
}

variable groups {
  description = "Array of { name, id } pairs identifying extra groups that should be created"
  default = []
}

variable "users" {
  description = "Information about users to create on the cluster"
  default = []
}

variable "host_count" {
  description = "Number of worker nodes"
  default = "1"
}

variable "globus_connect" {
  description = "Globus Connect setup parameters"
  default = {}
}
